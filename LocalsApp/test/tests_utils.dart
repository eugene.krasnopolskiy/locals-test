import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart' as di;
import 'package:locals_app/data/api/responses/auth/api_auth.dart';
import 'package:locals_app/data/api/responses/auth/api_auth_error.dart';
import 'package:locals_app/data/api/responses/base/api_base_response.dart';
import 'package:locals_app/domain/repository/api/api_auth_repository.dart';
import 'package:locals_app/domain/repository/general/device_repository.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';
import 'package:mockito/mockito.dart';

import 'data/api/api_auth_repository_impl_test.mocks.dart';

class TestsUtils {
  static const testEmail = "testlocals0@gmail.com";
  static const testPassword = "jahubhsgvd23";
  static const testDeviceId = "1234567890123456";

  Future<String> getSessionToken() async {
    String token = '';

    try {
      await GetIt.instance.reset();
      await configureDependencies(di.Environment.test);
      await GetIt.instance.allReady();
      GetIt.instance.allowReassignment = true;
      GetIt.instance.registerSingleton<DeviceRepository>(MockDeviceRepository());

      // Example of mock DeviceRepository
      final DeviceRepository deviceRepository = getIt<DeviceRepository>();
      when(deviceRepository.getDeviceId()).thenAnswer((_) => Future.value(TestsUtils.testDeviceId));

      // test
      final ApiAuthRepository underTest = getIt<ApiAuthRepository>();
      final ApiBaseResponse<ApiAuth, ApiAuthError> result = await underTest.authenticate(
          email: TestsUtils.testEmail, password: TestsUtils.testPassword, deviceId: await deviceRepository.getDeviceId());
      token = result.data?.authToken ?? '';
    } catch (_) {}

    return Future.value(token);
  }
}
