import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart' as di;
import 'package:locals_app/data/api/responses/base/api_base_response.dart';
import 'package:locals_app/data/api/responses/base/api_response_status.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_error.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_order.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_result.dart';
import 'package:locals_app/domain/repository/api/api_feed_repository.dart';
import 'package:locals_app/domain/repository/general/device_repository.dart';
import 'package:locals_app/domain/repository/secure/memory_secure_repository.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';
import 'package:mockito/annotations.dart';

import '../../tests_utils.dart';

final TestsUtils _testsUtils = TestsUtils();

@GenerateMocks([DeviceRepository])
void main() {
  late String authToken;

  Future<void> _prepareGeneralMocks() async {
    await GetIt.instance.reset();
    await configureDependencies(di.Environment.test);
    await GetIt.instance.allReady();

    final MemorySecureRepository emorySecureRepository = getIt<MemorySecureRepository>();
    emorySecureRepository.initialize(
      authToken: authToken,
      deviceId: TestsUtils.testDeviceId,
    );
  }

  setUpAll(() async {
    authToken = await _testsUtils.getSessionToken();
  });

  group('feed', () {
    test('getFeed success', () async {
      // prepare mock
      await _prepareGeneralMocks();

      // test
      final ApiFeedRepository underTest = getIt<ApiFeedRepository>();
      final ApiBaseResponse<ApiFeedResult, ApiFeedError> result = await underTest.getFeed(
        pageSize: 10,
        lastPostId: 0,
        order: ApiFeedOrder.recent,
      );

      // check
      expect(result.status, equals(ApiResponseStatus.success));
      expect(result.error, equals(null));
      expect(result.data, isNot(null));
      expect(result.data!.data.isNotEmpty, equals(true));
      expect(result.data!.data.length, lessThanOrEqualTo(10));

      for (final item in result.data!.data) {
        expect(item.id, greaterThanOrEqualTo(0));
        expect(item.authorId, greaterThanOrEqualTo(0));
        expect(item.communityId, greaterThanOrEqualTo(0));
        expect(item.timestamp, greaterThanOrEqualTo(0));
      }
    });

    test('authenticate error', () async {
      // prepare mock
      await _prepareGeneralMocks();
      final MemorySecureRepository emorySecureRepository = getIt<MemorySecureRepository>();
      emorySecureRepository.initialize(
        authToken: authToken,
        deviceId: TestsUtils.testDeviceId + '123',
      );

      // test
      final ApiFeedRepository underTest = getIt<ApiFeedRepository>();
      final ApiBaseResponse<ApiFeedResult, ApiFeedError> result = await underTest.getFeed(
        pageSize: 10,
        lastPostId: 0,
        order: ApiFeedOrder.recent,
      );

      // check
      expect(result.status, equals(ApiResponseStatus.error));
      expect(result.data, equals(null));
      expect(result.error, isNot(null));
      expect(result.error!.code, equals('NOT_AUTH'));
    });
  });
}
