import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart' as di;
import 'package:locals_app/data/api/responses/auth/api_auth.dart';
import 'package:locals_app/data/api/responses/auth/api_auth_error.dart';
import 'package:locals_app/data/api/responses/base/api_base_response.dart';
import 'package:locals_app/data/api/responses/base/api_response_status.dart';
import 'package:locals_app/domain/repository/api/api_auth_repository.dart';
import 'package:locals_app/domain/repository/general/device_repository.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../tests_utils.dart';
import 'api_auth_repository_impl_test.mocks.dart';

@GenerateMocks([DeviceRepository])
void main() {
  group('authenticate', () {
    test('authenticate success', () async {
      // prepare mock
      await GetIt.instance.reset();
      await configureDependencies(di.Environment.test);
      await GetIt.instance.allReady();
      GetIt.instance.allowReassignment = true;
      GetIt.instance.registerSingleton<DeviceRepository>(MockDeviceRepository());

      // Example of mock DeviceRepository
      final DeviceRepository deviceRepository = getIt<DeviceRepository>();
      when(deviceRepository.getDeviceId()).thenAnswer((_) => Future.value(TestsUtils.testDeviceId));

      // test
      final ApiAuthRepository underTest = getIt<ApiAuthRepository>();
      final ApiBaseResponse<ApiAuth, ApiAuthError> result = await underTest.authenticate(
          email: TestsUtils.testEmail, password: TestsUtils.testPassword, deviceId: await deviceRepository.getDeviceId());

      // check
      expect(result.status, equals(ApiResponseStatus.success));
      expect(result.error, equals(null));
      expect(result.data, isNot(null));
      expect(result.data!.email, equals(TestsUtils.testEmail));
      expect(result.data!.username.isNotEmpty, equals(true));
      expect(result.data!.uniqueId.isNotEmpty, equals(true));
      expect(result.data!.authToken.isNotEmpty, equals(true));
      expect(result.data!.userId, greaterThanOrEqualTo(0));
    });

    test('authenticate error', () async {
      // prepare mock
      await GetIt.instance.reset();
      await configureDependencies(di.Environment.test);
      await GetIt.instance.allReady();
      GetIt.instance.allowReassignment = true;
      GetIt.instance.registerSingleton<DeviceRepository>(MockDeviceRepository());

      // Example of mock DeviceRepository
      final DeviceRepository deviceRepository = getIt<DeviceRepository>();
      when(deviceRepository.getDeviceId()).thenAnswer((_) => Future.value(TestsUtils.testDeviceId));

      // test
      final ApiAuthRepository underTest = getIt<ApiAuthRepository>();
      final ApiBaseResponse<ApiAuth, ApiAuthError> result = await underTest.authenticate(
          email: TestsUtils.testPassword, password: TestsUtils.testEmail, deviceId: await deviceRepository.getDeviceId());

      // check
      expect(result.status, equals(ApiResponseStatus.error));
      expect(result.data, equals(null));
      expect(result.error, isNot(null));
      expect(result.error!.error.isNotEmpty, equals(true));
    });
  });
}
