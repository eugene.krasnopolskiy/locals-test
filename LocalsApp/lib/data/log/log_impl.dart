import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/log/log.dart';

@LazySingleton(as: Log)
class LogImpl implements Log {
  @override
  void d(String tag, String message) {
    debugPrint('DEBUG : $tag : $message');
  }

  @override
  void e(String tag, dynamic error, [dynamic stacktrace = '']) {
    debugPrint('ERROR : $tag : $error : $stacktrace');
  }
}
