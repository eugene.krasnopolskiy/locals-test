import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/connectivity/connectivity_repository.dart';

@Singleton(as: ConnectivityRepository)
class ConnectivityRepositoryImpl implements ConnectivityRepository {
  final Connectivity connectivity;
  final List<Function(ConnectivityResult)> _callbacks = [];

  StreamSubscription<ConnectivityResult>? _connectivitySubscription;
  ConnectivityResult _connectivityResult = ConnectivityResult.none;

  ConnectivityRepositoryImpl(this.connectivity);

  @override
  Future<void> init() async {
    if (_connectivitySubscription == null) {
      _connectivitySubscription = connectivity.onConnectivityChanged.listen(_notifyConnectionStatus);
      _connectivityResult = await connectivity.checkConnectivity();
    }
  }

  @override
  Future<void> release() async {
    if (_connectivitySubscription != null) {
      await _connectivitySubscription!.cancel();
      _connectivitySubscription = null;
      _connectivityResult = ConnectivityResult.none;
    }
  }

  @override
  void addListener(Function(ConnectivityResult value) listener) {
    if (!_callbacks.contains(listener)) {
      _callbacks.add(listener);
    }
  }

  @override
  ConnectivityResult getStatus() {
    return _connectivityResult;
  }

  @override
  void removeListener(Function(ConnectivityResult value) listener) {
    if (_callbacks.contains(listener)) {
      _callbacks.remove(listener);
    }
  }

  Future<void> _notifyConnectionStatus(ConnectivityResult result) async {
    if (result != _connectivityResult) {
      _connectivityResult = result;

      if (_callbacks.isNotEmpty) {
        for (final item in _callbacks) {
          item.call(result);
        }
      }
    }
  }
}
