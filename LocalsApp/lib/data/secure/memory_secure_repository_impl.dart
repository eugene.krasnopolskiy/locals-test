import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/secure/memory_secure_repository.dart';
import 'package:locals_app/general/log_helper.dart';

@Singleton(as: MemorySecureRepository)
class MemorySecureRepositoryImpl with LogHelper implements MemorySecureRepository {
  static const tag = 'MemorySecureRepositoryImpl';

  String? _authToken;

  String? _deviceId;

  @override
  String? get authToken => _authToken;

  @override
  void clean() {
    _authToken = null;
    _deviceId = null;
  }

  @override
  String? get deviceId => _deviceId;

  @override
  bool isExist() {
    return _authToken != null && _deviceId != null;
  }

  @override
  void initialize({
    required String authToken,
    required String deviceId,
  }) {
    _authToken = authToken;
    _deviceId = deviceId;
  }
}
