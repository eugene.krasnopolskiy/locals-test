import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/secure/secure_auth_repository.dart';
import 'package:locals_app/general/log_helper.dart';

@LazySingleton(as: SecureUserRepository)
class SecureUserRepositoryImpl with LogHelper implements SecureUserRepository {
  static const tag = 'SecureUserRepositoryImpl';

  static const _authTokenKey = 'auth_token';

  final FlutterSecureStorage _flutterSecureStorage;

  SecureUserRepositoryImpl(this._flutterSecureStorage);

  @override
  Future<String?> getAuthToken() {
    return _flutterSecureStorage.read(key: _authTokenKey);
  }

  @override
  Future<void> removeAuthToken() {
    return _flutterSecureStorage.delete(key: _authTokenKey);
  }

  @override
  Future<void> setAuthToken(String token) {
    return _flutterSecureStorage.write(key: _authTokenKey, value: token);
  }

  @override
  Future<bool> isAuthTokenExist() {
    return _flutterSecureStorage.containsKey(key: _authTokenKey);
  }
}
