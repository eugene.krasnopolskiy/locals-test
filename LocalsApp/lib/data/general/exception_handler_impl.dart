import 'dart:async';
import 'dart:collection';

import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/general/exception_handler.dart';
import 'package:locals_app/domain/repository/log/log.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';

@Singleton(as: ExceptionHandler)
class ExceptionHandlerImpl implements ExceptionHandler {
  static const tag = 'ExceptionHandlerImpl';

  final HashMap<String, ExceptionHandlerListener> _listeners = HashMap<String, ExceptionHandlerListener>();

  @override
  Future<void> processException(Object error, StackTrace? stack) async {
    final Log tLog = getIt<Log>();
    tLog.d(tag, 'processException, listeners counts: ${_listeners.length}');

    for (final item in _listeners.values) {
      _processError(item, error, stack);
    }

    return Future.value();
  }

  @override
  Future<void> addListener(String tag, ExceptionHandlerListener listener) async {
    if (_listeners.containsKey(tag)) {
      _listeners.remove(tag);
    }

    _listeners.putIfAbsent(tag, () => listener);
    return Future.value();
  }

  @override
  Future<void> removeListener(String tag) async {
    if (_listeners.containsKey(tag)) {
      _listeners.remove(tag);
    }

    return Future.value();
  }

  void _processError(ExceptionHandlerListener listener, Object error, StackTrace? stack) {
    listener(error, stack);
  }
}

typedef ExceptionHandlerListener = void Function(Object error, StackTrace? stack);
