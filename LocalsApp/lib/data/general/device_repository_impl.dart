import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/general/device_repository.dart';

@Singleton(as: DeviceRepository)
class DeviceRepositoryImpl implements DeviceRepository {
  static const tag = 'DeviceRepositoryImpl';

  final DeviceInfoPlugin _deviceInfoPlugin;

  DeviceRepositoryImpl(this._deviceInfoPlugin);

  @override
  Future<String> getDeviceId() async {
    if (Platform.isAndroid) {
      final AndroidDeviceInfo info = await _deviceInfoPlugin.androidInfo;
      return Future.value(info.androidId);
    } else if (Platform.isIOS) {
      final IosDeviceInfo info = await _deviceInfoPlugin.iosInfo;
      return Future.value(info.identifierForVendor);
    } else {
      return '';
    }
  }
}
