import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:locals_app/data/api/base/base_api_repository.dart';
import 'package:locals_app/data/api/responses/base/api_base_response.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_error.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_order.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_result.dart';
import 'package:locals_app/domain/repository/api/api_feed_repository.dart';
import 'package:locals_app/domain/repository/secure/memory_secure_repository.dart';
import 'package:locals_app/general/log_helper.dart';

@Singleton(as: ApiFeedRepository)
class ApiFeedRepositoryImpl extends BaseApiRepository with LogHelper implements ApiFeedRepository {
  static const tag = 'ApiFeedRepositoryImpl';

  static const _filedData = 'data';
  static const _filedPageSize = 'page_size';
  static const _filedOrder = 'order';
  static const _filedLastPostId = 'lpid';
  static const _codeSuccess = 'ok';
  static const _orderRecent = 'recent';
  static const _orderOldest = 'oldest';

  ApiFeedRepositoryImpl(MemorySecureRepository memorySecureRepository) : super(memorySecureRepository);

  @override
  String getBasePath() {
    return 'api/v1/posts/feed/global.json';
  }

  @override
  Future<ApiBaseResponse<ApiFeedResult, ApiFeedError>> getFeed({
    required int pageSize,
    required int lastPostId,
    required ApiFeedOrder order,
  }) async {
    ApiBaseResponse<ApiFeedResult, ApiFeedError>? result;

    final String url = getApiUrl();
    tLog.d(tag, 'authenticate, url: $url');

    final body = {
      _filedData: {
        _filedPageSize: pageSize,
        _filedLastPostId: lastPostId,
        _filedOrder: order == ApiFeedOrder.recent ? _orderRecent : _orderOldest,
      }
    };

    final response = await executeRequest(body: json.encode(body), requestType: RequestType.post, url: url);

    if (await validateResponse(response) == ValidateState.success) {
      final decodedBody = utf8.decode(response.bodyBytes);
      final map = json.decode(decodedBody) as Map<String, dynamic>;
      final data = ApiFeedResult.fromJson(map);

      if (data.code.toLowerCase() == _codeSuccess) {
        result = ApiBaseResponse.success(data: data);
      } else {
        result = ApiBaseResponse.error(error: ApiFeedError.fromJson(map));
      }
    }

    result ??= ApiBaseResponse.error();
    return Future.value(result);
  }
}
