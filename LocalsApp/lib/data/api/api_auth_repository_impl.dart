import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';
import 'package:locals_app/data/api/base/base_api_repository.dart';
import 'package:locals_app/data/api/responses/auth/api_auth.dart';
import 'package:locals_app/data/api/responses/auth/api_auth_error.dart';
import 'package:locals_app/data/api/responses/base/api_base_response.dart';
import 'package:locals_app/domain/repository/api/api_auth_repository.dart';
import 'package:locals_app/domain/repository/secure/memory_secure_repository.dart';
import 'package:locals_app/general/log_helper.dart';

@Singleton(as: ApiAuthRepository)
class ApiAuthRepositoryImpl extends BaseApiRepository with LogHelper implements ApiAuthRepository {
  static const tag = 'ApiRepositoryImpl';

  static const _filedDeviceId = 'device_id';
  static const _filedPassword = 'password';
  static const _filedEmail = 'email';
  static const _filedResult = 'result';
  static const _filedError = 'error';

  ApiAuthRepositoryImpl(MemorySecureRepository memorySecureRepository) : super(memorySecureRepository);

  @override
  String getBasePath() {
    return 'app_api/auth.php';
  }

  @override
  Future<ApiBaseResponse<ApiAuth, ApiAuthError>> authenticate({
    required String email,
    required String password,
    required String deviceId,
  }) async {
    ApiBaseResponse<ApiAuth, ApiAuthError>? result;

    final String url = getApiUrl();
    tLog.d(tag, 'authenticate, url: $url');

    final request = http.Request(BaseApiRepository.methodPost, Uri.parse(url));
    request.bodyFields = {
      _filedDeviceId: deviceId,
      _filedPassword: password,
      _filedEmail: email,
    };
    final sendRequest = await request.send();
    final response = await http.Response.fromStream(sendRequest);

    if (await validateResponse(response) == ValidateState.success) {
      final decodedBody = utf8.decode(response.bodyBytes);
      final map = json.decode(decodedBody) as Map<String, dynamic>;

      if (map.containsKey(_filedResult) && map[_filedResult] is Map<String, dynamic>) {
        result = ApiBaseResponse.success(data: ApiAuth.fromJson(map[_filedResult]));
      } else if (map.containsKey(_filedError)) {
        result = ApiBaseResponse.error(error: ApiAuthError.fromJson(map));
      }
    }

    result ??= ApiBaseResponse.error();
    return Future.value(result);
  }
}
