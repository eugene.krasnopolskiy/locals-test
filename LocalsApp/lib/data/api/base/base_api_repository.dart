import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:locals_app/domain/repository/secure/memory_secure_repository.dart';
import 'package:locals_app/general/constants.dart';
import 'package:locals_app/general/log_helper.dart';

abstract class BaseApiRepository with LogHelper {
  static const tag = 'BaseApiRepository';
  static const methodPost = 'POST';
  static const defaultTimeout = Duration(seconds: Constants.networkRequestsTimeout);

  static const deviceId = "X-DEVICE-ID";
  static const authToken = "X-APP-AUTH-TOKEN";

  final MemorySecureRepository memorySecureRepository;

  BaseApiRepository(this.memorySecureRepository);

  String get baseApiUrl => Constants.apiUrl;

  String getBasePath();

  String getApiUrl({String? apiName}) {
    return baseApiUrl + getBasePath() + (apiName ?? '');
  }

  Future<ValidateState> validateResponse(http.Response response) async {
    tLog.d(tag, 'validateResponse, response code: ${response.statusCode}');
    if (response.statusCode >= 200 && response.statusCode <= 299) {
      return Future.value(ValidateState.success);
    } else {
      return Future.value(ValidateState.fail);
    }
  }

  Future<http.Response> executeRequest({
    required RequestType requestType,
    required String url,
    Object? body,
    Map<String, String>? header,
  }) async {
    final uri = Uri.parse(Uri.encodeFull(url));
    final Map<String, String> headers = header ?? _prepareHeader();
    http.Response response;

    switch (requestType) {
      case RequestType.get:
        response = await http.get(uri, headers: headers).timeout(defaultTimeout);
        break;
      case RequestType.post:
        response = await http.post(uri, body: body, headers: headers).timeout(defaultTimeout);
        break;
      case RequestType.put:
        response = await http.put(uri, body: body, headers: headers).timeout(defaultTimeout);
        break;
      case RequestType.patch:
        response = await http.patch(uri, body: body, headers: headers).timeout(defaultTimeout);
        break;
      case RequestType.delete:
        response = await http.delete(uri, headers: headers).timeout(defaultTimeout);
        break;
    }

    return Future.value(response);
  }

  Map<String, String> _prepareHeader() {
    final Map<String, String> headers = <String, String>{};
    headers[HttpHeaders.contentTypeHeader] = 'application/json; charset=utf-8';
    headers[authToken] = memorySecureRepository.authToken!;
    headers[deviceId] = memorySecureRepository.deviceId!;
    return headers;
  }
}

enum ValidateState { success, fail }
enum RequestType { get, post, put, patch, delete }
