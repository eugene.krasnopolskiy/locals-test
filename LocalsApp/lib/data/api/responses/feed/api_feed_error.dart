import 'package:locals_app/general/parse_data_helper.dart';

class ApiFeedError {
  final int status;
  final String code;
  final List errors;
  final List params;

  ApiFeedError({
    required this.status,
    required this.code,
    required this.errors,
    required this.params,
  });

  factory ApiFeedError.fromJson(Map<String, dynamic> json) {
    return ApiFeedError(
      status: ParseDataHelper.parseIntWithNaN(json, 'status'),
      code: ParseDataHelper.parseString(json, 'code'),
      errors: [],
      params: [],
    );
  }
}
