import 'package:locals_app/data/api/responses/feed/api_feed_item.dart';
import 'package:locals_app/general/parse_data_helper.dart';

class ApiFeedResult {
  final int status;
  final String code;
  final List<ApiFeedItem> data;

  ApiFeedResult({required this.status, required this.code, required this.data});

  factory ApiFeedResult.fromJson(Map<String, dynamic> json) {
    final List<ApiFeedItem> items = [];

    if (json.containsKey('data') && json['data'] is List) {
      items.addAll((json['data'] as List).map((e) => ApiFeedItem.fromJson(e)));
    }

    return ApiFeedResult(
      data: items,
      status: ParseDataHelper.parseIntWithNaN(json, 'status'),
      code: ParseDataHelper.parseString(json, 'code'),
    );
  }
}
