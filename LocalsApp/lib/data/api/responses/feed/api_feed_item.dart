import 'package:locals_app/general/parse_data_helper.dart';

class ApiFeedItem {
  final int id;
  final int authorId;
  final int communityId;
  final String text;
  final String title;
  final bool likedByUs;
  final bool commentedByUs;
  final bool bookmarked;
  final int timestamp;
  final int totalPostViews;
  final bool isBlured;
  final String authorName;
  final String authorAvatarExtension;
  final String authorAvatarUrl;

  ApiFeedItem({
    required this.id,
    required this.authorId,
    required this.communityId,
    required this.text,
    required this.title,
    required this.likedByUs,
    required this.commentedByUs,
    required this.bookmarked,
    required this.timestamp,
    required this.totalPostViews,
    required this.isBlured,
    required this.authorName,
    required this.authorAvatarExtension,
    required this.authorAvatarUrl,
  });

  factory ApiFeedItem.fromJson(Map<String, dynamic> json) {
    return ApiFeedItem(
      id: ParseDataHelper.parseIntWithNaN(json, 'id'),
      authorId: ParseDataHelper.parseIntWithNaN(json, 'author_id'),
      communityId: ParseDataHelper.parseIntWithNaN(json, 'community_id'),
      text: ParseDataHelper.parseString(json, 'text'),
      title: ParseDataHelper.parseString(json, 'title'),
      likedByUs: ParseDataHelper.parseBool(json: json, fieldName: 'liked_by_us'),
      commentedByUs: ParseDataHelper.parseBool(json: json, fieldName: 'commented_by_us'),
      bookmarked: ParseDataHelper.parseBool(json: json, fieldName: 'bookmarked'),
      timestamp: ParseDataHelper.parseIntWithNaN(json, 'timestamp'),
      totalPostViews: ParseDataHelper.parseIntWithNaN(json, 'total_post_views'),
      isBlured: ParseDataHelper.parseBool(json: json, fieldName: 'is_blured'),
      authorName: ParseDataHelper.parseString(json, 'author_name'),
      authorAvatarExtension: ParseDataHelper.parseString(json, 'author_avatar_extension'),
      authorAvatarUrl: ParseDataHelper.parseString(json, 'author_avatar_url'),
    );
  }
}
