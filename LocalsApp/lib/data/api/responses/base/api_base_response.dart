import 'package:locals_app/data/api/responses/base/api_response_status.dart';

class ApiBaseResponse<T, E> {
  ApiResponseStatus status;

  final T? data;

  final E? error;

  ApiBaseResponse({required this.status, this.data, this.error});

  factory ApiBaseResponse.success({T? data}) {
    return ApiBaseResponse(status: ApiResponseStatus.success, data: data);
  }

  factory ApiBaseResponse.error({E? error}) {
    return ApiBaseResponse(status: ApiResponseStatus.error, error: error);
  }

  factory ApiBaseResponse.status(ApiResponseStatus status) {
    return ApiBaseResponse(status: status);
  }
}
