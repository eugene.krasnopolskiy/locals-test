class ApiResult {
  final Map<String, dynamic> data;

  ApiResult({required this.data});

  factory ApiResult.fromJson(Map<String, dynamic> json) {
    return ApiResult(
      data: json.containsKey('result') && json['result'] is Map<String, dynamic>
          ? json['result'] as Map<String, dynamic>
          : <String, dynamic>{},
    );
  }
}
