import 'package:locals_app/general/parse_data_helper.dart';

class ApiAuthError {
  final String error;

  ApiAuthError({required this.error});

  factory ApiAuthError.fromJson(Map<String, dynamic> json) {
    return ApiAuthError(error: ParseDataHelper.parseString(json, 'error'));
  }
}
