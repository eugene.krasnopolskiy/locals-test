import 'package:locals_app/general/parse_data_helper.dart';

class ApiAuth {
  final int userId;
  final int activeSubscriber;
  final int unclaimedGift;
  final String username;
  final String email;
  final String uniqueId;
  final String authToken;

  ApiAuth({
    required this.userId,
    required this.activeSubscriber,
    required this.unclaimedGift,
    required this.username,
    required this.email,
    required this.uniqueId,
    required this.authToken,
  });

  factory ApiAuth.fromJson(Map<String, dynamic> json) {
    return ApiAuth(
      userId: ParseDataHelper.parseIntWithNaN(json, 'user_id'),
      activeSubscriber: ParseDataHelper.parseIntWithNaN(json, 'active_subscriber'),
      unclaimedGift: ParseDataHelper.parseIntWithNaN(json, 'unclaimed_gift'),
      username: ParseDataHelper.parseString(json, 'username'),
      email: ParseDataHelper.parseString(json, 'email'),
      uniqueId: ParseDataHelper.parseString(json, 'unique_id'),
      authToken: ParseDataHelper.parseString(json, 'ss_auth_token'),
    );
  }
}
