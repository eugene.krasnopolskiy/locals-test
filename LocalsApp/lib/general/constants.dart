import 'package:flutter/services.dart';

class Constants {
  // General
  static const splashScreenDelay = 2; // seconds
  static const networkRequestsTimeout = 20; // 20 seconds
  static const feedPageSize = 10; // items

  // list of default orientation
  static const List<DeviceOrientation> orientations = [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown];

  // Urls
  static const apiUrl = 'https://app-test.rr-qa.seasteaddigital.com/';

  // Test credentials
  static const testEmail = "testlocals0@gmail.com";
  static const testPassword = "jahubhsgvd23";
}
