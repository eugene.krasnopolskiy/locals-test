import 'package:locals_app/domain/repository/log/log.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';

mixin LogHelper {
  final Log tLog = getIt<Log>();
}
