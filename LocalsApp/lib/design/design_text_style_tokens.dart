import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:locals_app/design/app_colors.dart';

@Singleton()
class DesignTextStyleTokens {
  TextStyle splashTitle() {
    return const TextStyle(
      color: AppColors.textMainColor,
      fontWeight: FontWeight.w700,
      fontSize: 42.0,
    );
  }

  TextStyle buttonActiveTitle() {
    return const TextStyle(
      color: Colors.black,
      fontSize: 20.0,
    );
  }

  TextStyle buttonInactiveTitle() {
    return const TextStyle(
      color: AppColors.textMainColor,
      fontSize: 20.0,
    );
  }

  TextStyle textField() {
    return const TextStyle(
      color: AppColors.textMainColor,
      fontSize: 16.0,
    );
  }

  TextStyle feedTitle() {
    return const TextStyle(
      color: AppColors.textMainColor,
      fontWeight: FontWeight.w700,
      fontSize: 20.0,
    );
  }

  TextStyle feedText() {
    return const TextStyle(
      color: AppColors.textMainColor,
      fontSize: 16.0,
    );
  }

  TextStyle feedAuthorName() {
    return const TextStyle(
      color: AppColors.textSecondaryColor,
      fontSize: 16.0,
    );
  }

  TextStyle feedInfo() {
    return const TextStyle(
      color: AppColors.textSecondaryColor,
      fontSize: 14.0,
    );
  }

  TextStyle feedTime() {
    return const TextStyle(
      color: AppColors.textSecondaryColor,
      fontSize: 12.0,
    );
  }

  TextStyle recentSort() {
    return const TextStyle(
      color: AppColors.textSecondaryColor,
      fontSize: 20.0,
    );
  }

  TextStyle recentSortType() {
    return const TextStyle(
      color: AppColors.textMainColor,
      fontSize: 20.0,
    );
  }
}
