import 'package:injectable/injectable.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_item.dart';
import 'package:locals_app/domain/entity/feed_item.dart';
import 'package:locals_app/domain/mapper/base/mapper.dart';

@LazySingleton()
class FeedItemMapper extends Mapper<ApiFeedItem, FeedItem> {
  @override
  ApiFeedItem reverseMap(FeedItem value) {
    throw Exception('Not supported');
  }

  @override
  FeedItem map(ApiFeedItem value) {
    return FeedItem(
      id: value.id,
      authorId: value.authorId,
      communityId: value.communityId,
      text: value.text,
      title: value.title,
      likedByUs: value.likedByUs,
      commentedByUs: value.commentedByUs,
      bookmarked: value.bookmarked,
      timestamp: DateTime.fromMillisecondsSinceEpoch(value.timestamp * 1000),
      totalPostViews: value.totalPostViews,
      isBlured: value.isBlured,
      authorName: value.authorName,
      authorAvatarExtension: value.authorAvatarExtension,
      authorAvatarUrl: value.authorAvatarUrl,
    );
  }
}
