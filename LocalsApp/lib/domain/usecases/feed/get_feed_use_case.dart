import 'package:injectable/injectable.dart';
import 'package:locals_app/data/api/responses/base/api_response_status.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_order.dart';
import 'package:locals_app/domain/entity/feed_item.dart';
import 'package:locals_app/domain/entity/feed_order.dart';
import 'package:locals_app/domain/mapper/feed_item_mapper.dart';
import 'package:locals_app/domain/repository/api/api_feed_repository.dart';
import 'package:locals_app/general/constants.dart';
import 'package:locals_app/general/log_helper.dart';

@LazySingleton()
class GetFeedUseCase with LogHelper {
  static const tag = 'SignInUseCase';

  final ApiFeedRepository _apiFeedRepository;
  final FeedItemMapper _feedItemMapper;

  GetFeedUseCase(
    this._apiFeedRepository,
    this._feedItemMapper,
  );

  Future<List<FeedItem>> executeAsync({FeedItem? lastFeed, FeedOrder order = FeedOrder.recent}) async {
    tLog.d(tag, 'GetFeedUseCase executeAsync');
    final List<FeedItem> result = [];
    final ApiFeedOrder apiOrder = order == FeedOrder.recent ? ApiFeedOrder.recent : ApiFeedOrder.oldest;
    final response = await _apiFeedRepository.getFeed(pageSize: Constants.feedPageSize, lastPostId: lastFeed?.id ?? 0, order: apiOrder);

    if (response.status == ApiResponseStatus.success && response.data != null) {
      result.addAll(_feedItemMapper.mapList(response.data!.data));
    }

    return Future.value(result);
  }
}
