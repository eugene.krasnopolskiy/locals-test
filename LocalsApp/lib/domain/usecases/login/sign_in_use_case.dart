import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/general/device_repository.dart';
import 'package:locals_app/domain/repository/secure/memory_secure_repository.dart';
import 'package:locals_app/domain/repository/secure/secure_auth_repository.dart';
import 'package:locals_app/general/log_helper.dart';

@LazySingleton()
class SignInUseCase with LogHelper {
  static const tag = 'SignInUseCase';

  final DeviceRepository _deviceRepository;
  final MemorySecureRepository _memorySecureRepository;
  final SecureUserRepository _secureUserRepository;

  SignInUseCase(
    this._deviceRepository,
    this._memorySecureRepository,
    this._secureUserRepository,
  );

  Future<bool> executeAsync() async {
    bool result = false;
    tLog.d(tag, 'LoginUseCase executeAsync');

    if (await _secureUserRepository.isAuthTokenExist()) {
      final deviceId = await _deviceRepository.getDeviceId();
      final authToken = await _secureUserRepository.getAuthToken() ?? '';
      _memorySecureRepository.initialize(authToken: authToken, deviceId: deviceId);
      result = true;
    }

    return Future.value(result);
  }
}
