import 'package:injectable/injectable.dart';
import 'package:locals_app/data/api/responses/auth/api_auth.dart';
import 'package:locals_app/data/api/responses/auth/api_auth_error.dart';
import 'package:locals_app/data/api/responses/base/api_base_response.dart';
import 'package:locals_app/data/api/responses/base/api_response_status.dart';
import 'package:locals_app/domain/repository/api/api_auth_repository.dart';
import 'package:locals_app/domain/repository/general/device_repository.dart';
import 'package:locals_app/domain/repository/secure/memory_secure_repository.dart';
import 'package:locals_app/domain/repository/secure/secure_auth_repository.dart';
import 'package:locals_app/general/log_helper.dart';

@LazySingleton()
class LoginUseCase with LogHelper {
  static const tag = 'LoginUseCase';

  final ApiAuthRepository _apiAuthRepository;
  final DeviceRepository _deviceRepository;
  final MemorySecureRepository _memorySecureRepository;
  final SecureUserRepository _secureUserRepository;

  LoginUseCase(
    this._apiAuthRepository,
    this._deviceRepository,
    this._memorySecureRepository,
    this._secureUserRepository,
  );

  Future<bool> executeAsync({required String email, required String password}) async {
    bool result = false;

    tLog.d(tag, 'LoginUseCase executeAsync');
    final deviceId = await _deviceRepository.getDeviceId();
    final ApiBaseResponse<ApiAuth, ApiAuthError> response =
        await _apiAuthRepository.authenticate(email: email, password: password, deviceId: deviceId);

    if (response.status == ApiResponseStatus.success && response.data != null) {
      await _secureUserRepository.setAuthToken(response.data!.authToken);
      _memorySecureRepository.initialize(authToken: response.data!.authToken, deviceId: deviceId);
      result = true;
    }

    return Future.value(result);
  }
}
