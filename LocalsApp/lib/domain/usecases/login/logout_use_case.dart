import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/secure/memory_secure_repository.dart';
import 'package:locals_app/domain/repository/secure/secure_auth_repository.dart';
import 'package:locals_app/general/log_helper.dart';

@LazySingleton()
class LogoutUseCase with LogHelper {
  static const tag = 'LoginUseCase';

  final MemorySecureRepository _memorySecureRepository;
  final SecureUserRepository _secureUserRepository;

  LogoutUseCase(
    this._memorySecureRepository,
    this._secureUserRepository,
  );

  Future<void> executeAsync() async {
    tLog.d(tag, 'LogoutUseCase executeAsync');
    await _secureUserRepository.removeAuthToken();
    _memorySecureRepository.clean();
    return Future.value();
  }
}
