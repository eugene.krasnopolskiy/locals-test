class FeedItem {
  final int id;
  final int authorId;
  final int communityId;
  final String text;
  final String title;
  final bool likedByUs;
  final bool commentedByUs;
  final bool bookmarked;
  final DateTime timestamp;
  final int totalPostViews;
  final bool isBlured;
  final String authorName;
  final String authorAvatarExtension;
  final String authorAvatarUrl;

  FeedItem({
    required this.id,
    required this.authorId,
    required this.communityId,
    required this.text,
    required this.title,
    required this.likedByUs,
    required this.commentedByUs,
    required this.bookmarked,
    required this.timestamp,
    required this.totalPostViews,
    required this.isBlured,
    required this.authorName,
    required this.authorAvatarExtension,
    required this.authorAvatarUrl,
  });
}
