import 'package:connectivity_plus/connectivity_plus.dart';

abstract class ConnectivityRepository {
  Future<void> init();

  Future<void> release();

  ConnectivityResult getStatus();

  void addListener(Function(ConnectivityResult) listener);

  void removeListener(Function(ConnectivityResult) listener);
}
