abstract class DeviceRepository {
  Future<String> getDeviceId();
}
