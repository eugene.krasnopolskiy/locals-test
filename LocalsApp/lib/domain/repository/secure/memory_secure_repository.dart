abstract class MemorySecureRepository {
  String? get authToken;

  String? get deviceId;

  void initialize({
    required String authToken,
    required String deviceId,
  });

  bool isExist();

  void clean();
}
