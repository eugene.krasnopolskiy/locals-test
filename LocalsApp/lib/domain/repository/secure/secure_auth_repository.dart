abstract class SecureUserRepository {
  Future<bool> isAuthTokenExist();

  Future<void> setAuthToken(String token);

  Future<String?> getAuthToken();

  Future<void> removeAuthToken();
}
