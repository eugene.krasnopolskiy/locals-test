import 'package:locals_app/data/api/responses/auth/api_auth.dart';
import 'package:locals_app/data/api/responses/auth/api_auth_error.dart';
import 'package:locals_app/data/api/responses/base/api_base_response.dart';

abstract class ApiAuthRepository {
  Future<ApiBaseResponse<ApiAuth, ApiAuthError>> authenticate({required String email, required String password, required String deviceId});
}
