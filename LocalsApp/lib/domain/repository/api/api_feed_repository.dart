import 'package:locals_app/data/api/responses/base/api_base_response.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_error.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_order.dart';
import 'package:locals_app/data/api/responses/feed/api_feed_result.dart';

abstract class ApiFeedRepository {
  Future<ApiBaseResponse<ApiFeedResult, ApiFeedError>> getFeed(
      {required int pageSize, required int lastPostId, required ApiFeedOrder order});
}
