import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.config.dart';

final getIt = GetIt.instance;

@InjectableInit()
Future<void> configureDependencies(String env) async {
  $initGetIt(getIt, environment: env);
  return Future.value();
}
