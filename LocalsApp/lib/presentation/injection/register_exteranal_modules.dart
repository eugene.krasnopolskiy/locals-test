import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

@module
abstract class RegisterExternalModules {
  FlutterSecureStorage get flutterSecureStorage => const FlutterSecureStorage();

  DeviceInfoPlugin get deviceInfoPlugin => DeviceInfoPlugin();

  Connectivity get connectivity => Connectivity();
}
