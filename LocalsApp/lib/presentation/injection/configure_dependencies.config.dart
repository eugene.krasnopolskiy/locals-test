// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:connectivity_plus/connectivity_plus.dart' as _i3;
import 'package:device_info_plus/device_info_plus.dart' as _i7;
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as _i13;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../data/api/api_auth_repository_impl.dart' as _i22;
import '../../data/api/api_feed_repository_impl.dart' as _i24;
import '../../data/connectivity/connectivity_repository_impl.dart' as _i5;
import '../../data/general/device_repository_impl.dart' as _i9;
import '../../data/general/exception_handler_impl.dart' as _i11;
import '../../data/log/log_impl.dart' as _i15;
import '../../data/secure/memory_secure_repository_impl.dart' as _i17;
import '../../data/secure/secure_auth_repository_impl.dart' as _i19;
import '../../design/design_text_style_tokens.dart' as _i6;
import '../../domain/mapper/feed_item_mapper.dart' as _i12;
import '../../domain/repository/api/api_auth_repository.dart' as _i21;
import '../../domain/repository/api/api_feed_repository.dart' as _i23;
import '../../domain/repository/connectivity/connectivity_repository.dart'
    as _i4;
import '../../domain/repository/general/device_repository.dart' as _i8;
import '../../domain/repository/general/exception_handler.dart' as _i10;
import '../../domain/repository/log/log.dart' as _i14;
import '../../domain/repository/secure/memory_secure_repository.dart' as _i16;
import '../../domain/repository/secure/secure_auth_repository.dart' as _i18;
import '../../domain/usecases/feed/get_feed_use_case.dart' as _i25;
import '../../domain/usecases/login/login_use_case.dart' as _i26;
import '../../domain/usecases/login/logout_use_case.dart' as _i27;
import '../../domain/usecases/login/sign_in_use_case.dart' as _i20;
import 'register_exteranal_modules.dart'
    as _i28; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final registerExternalModules = _$RegisterExternalModules();
  gh.factory<_i3.Connectivity>(() => registerExternalModules.connectivity);
  gh.singleton<_i4.ConnectivityRepository>(
      _i5.ConnectivityRepositoryImpl(get<_i3.Connectivity>()));
  gh.singleton<_i6.DesignTextStyleTokens>(_i6.DesignTextStyleTokens());
  gh.factory<_i7.DeviceInfoPlugin>(
      () => registerExternalModules.deviceInfoPlugin);
  gh.singleton<_i8.DeviceRepository>(
      _i9.DeviceRepositoryImpl(get<_i7.DeviceInfoPlugin>()));
  gh.singleton<_i10.ExceptionHandler>(_i11.ExceptionHandlerImpl());
  gh.lazySingleton<_i12.FeedItemMapper>(() => _i12.FeedItemMapper());
  gh.factory<_i13.FlutterSecureStorage>(
      () => registerExternalModules.flutterSecureStorage);
  gh.lazySingleton<_i14.Log>(() => _i15.LogImpl());
  gh.singleton<_i16.MemorySecureRepository>(_i17.MemorySecureRepositoryImpl());
  gh.lazySingleton<_i18.SecureUserRepository>(
      () => _i19.SecureUserRepositoryImpl(get<_i13.FlutterSecureStorage>()));
  gh.lazySingleton<_i20.SignInUseCase>(() => _i20.SignInUseCase(
      get<_i8.DeviceRepository>(),
      get<_i16.MemorySecureRepository>(),
      get<_i18.SecureUserRepository>()));
  gh.singleton<_i21.ApiAuthRepository>(
      _i22.ApiAuthRepositoryImpl(get<_i16.MemorySecureRepository>()));
  gh.singleton<_i23.ApiFeedRepository>(
      _i24.ApiFeedRepositoryImpl(get<_i16.MemorySecureRepository>()));
  gh.lazySingleton<_i25.GetFeedUseCase>(() => _i25.GetFeedUseCase(
      get<_i23.ApiFeedRepository>(), get<_i12.FeedItemMapper>()));
  gh.lazySingleton<_i26.LoginUseCase>(() => _i26.LoginUseCase(
      get<_i21.ApiAuthRepository>(),
      get<_i8.DeviceRepository>(),
      get<_i16.MemorySecureRepository>(),
      get<_i18.SecureUserRepository>()));
  gh.lazySingleton<_i27.LogoutUseCase>(() => _i27.LogoutUseCase(
      get<_i16.MemorySecureRepository>(), get<_i18.SecureUserRepository>()));
  return get;
}

class _$RegisterExternalModules extends _i28.RegisterExternalModules {}
