import 'package:flutter/material.dart';
import 'package:locals_app/design/app_colors.dart';
import 'package:locals_app/presentation/ui/screens/splash_screen.dart';

class LocalsApp extends StatelessWidget {
  const LocalsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Locals',
      theme: ThemeData(
        primarySwatch: AppColors.createMaterialColor(AppColors.backgroundMainColor),
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
        primarySwatch: AppColors.createMaterialColor(AppColors.backgroundMainColor),
        brightness: Brightness.dark,
      ),
      themeMode: ThemeMode.dark,
      debugShowCheckedModeBanner: false,
      home: const SplashScreen(),
    );
  }
}