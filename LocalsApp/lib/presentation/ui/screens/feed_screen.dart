import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:locals_app/design/app_colors.dart';
import 'package:locals_app/design/design_text_style_tokens.dart';
import 'package:locals_app/domain/entity/feed_item.dart';
import 'package:locals_app/domain/entity/feed_order.dart';
import 'package:locals_app/domain/repository/connectivity/connectivity_repository.dart';
import 'package:locals_app/domain/usecases/feed/get_feed_use_case.dart';
import 'package:locals_app/domain/usecases/login/logout_use_case.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';
import 'package:locals_app/presentation/ui/screens/login_screen.dart';
import 'package:locals_app/presentation/ui/widgets/feed_item_widget.dart';

class FeedScreen extends StatefulWidget {
  static const tag = 'FeedScreen';

  const FeedScreen({Key? key}) : super(key: key);

  @override
  _FeedScreenState createState() {
    return _FeedScreenState();
  }
}

class _FeedScreenState extends State<FeedScreen> {
  final _designTextStyleTokens = getIt<DesignTextStyleTokens>();
  final _signInUseCase = getIt<GetFeedUseCase>();
  final _logoutUseCase = getIt<LogoutUseCase>();
  final _connectivityRepository = getIt<ConnectivityRepository>();
  final _scrollController = ScrollController();

  final List<FeedItem> _items = [];

  bool _loading = false;
  bool _waitAutoScroll = false;
  bool _logoutInProgress = false;
  FeedOrder _order = FeedOrder.recent;
  bool _connectionError = false;

  @override
  void initState() {
    super.initState();
    _connectionError = _connectivityRepository.getStatus() == ConnectivityResult.none;
    _connectivityRepository.addListener(onConnectionChanged);

    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if (!_waitAutoScroll) {
          _loadData();

          // smooth scroll to bottom of list
          _smoothScrollToBottomList();
        }
      }
    });

    _loadData();
  }

  @override
  void dispose() {
    _connectivityRepository.removeListener(onConnectionChanged);
    super.dispose();
  }

  void onConnectionChanged(ConnectivityResult value) {
    setState(() {
      _connectionError = value == ConnectivityResult.none;

      if (!_connectionError && _items.isEmpty) {
        _loadData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundMainColor,
      appBar: AppBar(
        title: const Text('Feed'),
        actions: [
          Row(
            children: [
              const Padding(
                padding: EdgeInsets.only(right: 8),
                child: Text("Sort by:", style: TextStyle(color: AppColors.textSecondaryColor)),
              ),
              DropdownButton<FeedOrder>(
                value: _order,
                style: const TextStyle(color: AppColors.textMainColor),
                onChanged: (FeedOrder? newValue) {
                  setState(() {
                    final value = newValue ?? FeedOrder.recent;

                    if (value != _order) {
                      _order = value;
                      _items.clear();
                      _loading = false;
                      _loadData();
                    }
                  });
                },
                items: <FeedOrder>[FeedOrder.recent, FeedOrder.oldest].map<DropdownMenuItem<FeedOrder>>((FeedOrder value) {
                  return DropdownMenuItem<FeedOrder>(
                    value: value,
                    child: Text(_getOrderName(value)),
                  );
                }).toList(),
              ),
            ],
          ),
          PopupMenuButton(itemBuilder: (context) {
            return [
              const PopupMenuItem<int>(
                value: 0,
                child: Text("Logout"),
              ),
            ];
          }, onSelected: (value) {
            if (value == 0) {
              _logout();
            }
          }),
        ],
      ),
      resizeToAvoidBottomInset: false,
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    if (_connectionError && _items.isEmpty) {
      return _buildConnectionError();
    } else if (_loading && _items.isEmpty) {
      return _buildProgress();
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (_connectionError) ...[
            Container(
              padding: const EdgeInsets.all(16),
              decoration: const BoxDecoration(color: AppColors.textAccentColor),
              alignment: Alignment.center,
              child: Text(
                'No internet connection',
                style: _designTextStyleTokens.buttonInactiveTitle(),
              ),
            )
          ],
          Expanded(
            child: _buildList(),
          ),
        ],
      );
    }
  }

  Widget _buildList() {
    return ListView.separated(
      controller: _scrollController,
      padding: const EdgeInsets.all(8),
      itemCount: _items.length + 1,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        if (index == _items.length) {
          if (_connectionError) {
            return Container();
          } else {
            return Container(
              margin: const EdgeInsets.all(16),
              child: const Center(
                child: SizedBox(
                  width: 24,
                  height: 24,
                  child: CircularProgressIndicator(
                    color: AppColors.textMainColor,
                    strokeWidth: 2,
                  ),
                ),
              ),
            );
          }
        } else {
          return FeedItemWidget(
            designTextStyleTokens: _designTextStyleTokens,
            feedItem: _items[index],
          );
        }
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }

  Widget _buildConnectionError() {
    return Center(
      child: Text(
        'No internet connection',
        style: _designTextStyleTokens.buttonInactiveTitle(),
      ),
    );
  }

  Widget _buildProgress() {
    return const Center(
      child: SizedBox(
        width: 48,
        height: 48,
        child: CircularProgressIndicator(
          color: AppColors.textMainColor,
          strokeWidth: 2,
        ),
      ),
    );
  }

  Future<void> _loadData() async {
    if (!_loading && !_connectionError) {
      setState(() {
        _loading = true;
      });

      await _signInUseCase.executeAsync(lastFeed: _items.isNotEmpty ? _items.last : null, order: _order).then((value) {
        setState(() {
          _items.addAll(value);
        });
      }).whenComplete(() {
        setState(() {
          _loading = false;
        });
      });
    }
  }

  void _smoothScrollToBottomList() {
    // smooth scroll to bottom of list
    _waitAutoScroll = true;
    Future.delayed(const Duration(milliseconds: 300)).whenComplete(() {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
      _waitAutoScroll = false;
    });
  }

  void _logout() {
    if (!_logoutInProgress) {
      _logoutInProgress = true;
      _logoutUseCase.executeAsync().whenComplete(() {
        _logoutInProgress = false;
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen()));
      });
    }
  }

  String _getOrderName(FeedOrder order) {
    switch (order) {
      case FeedOrder.recent:
        return 'Recent';
      case FeedOrder.oldest:
        return 'Olders';
    }
  }
}
