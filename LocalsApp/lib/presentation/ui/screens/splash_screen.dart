import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:locals_app/design/app_colors.dart';
import 'package:locals_app/design/design_text_style_tokens.dart';
import 'package:locals_app/domain/usecases/login/sign_in_use_case.dart';
import 'package:locals_app/general/constants.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';
import 'package:locals_app/presentation/ui/screens/feed_screen.dart';
import 'package:locals_app/presentation/ui/screens/login_screen.dart';

class SplashScreen extends StatefulWidget {
  static const tag = 'SplashScreen';

  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  final _designTextStyleTokens = getIt<DesignTextStyleTokens>();
  final _signInUseCase = getIt<SignInUseCase>();

  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.backgroundMainColor,
        systemNavigationBarDividerColor: AppColors.backgroundMainColor,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
      ),
      child: Scaffold(
        backgroundColor: AppColors.backgroundMainColor,
        resizeToAvoidBottomInset: false,
        body: Center(
          child: Text(
            'Locals Test',
            style: _designTextStyleTokens.splashTitle(),
          ),
        ),
      ),
    );
  }

  void _startTimer() {
    Future.delayed(const Duration(seconds: Constants.splashScreenDelay), () {
      _signInUseCase.executeAsync().then((value) {
        if (value) {
          // user sign in already, open feed screen
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const FeedScreen()));
        } else {
          // user not sing in, open login screen
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen()));
        }
      });
    });
  }
}
