import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:locals_app/design/app_colors.dart';
import 'package:locals_app/design/design_text_style_tokens.dart';
import 'package:locals_app/domain/repository/connectivity/connectivity_repository.dart';
import 'package:locals_app/domain/usecases/login/login_use_case.dart';
import 'package:locals_app/general/constants.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';
import 'package:locals_app/presentation/ui/screens/feed_screen.dart';

class LoginScreen extends StatefulWidget {
  static const tag = 'LoginScreen';

  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State<LoginScreen> {
  final _designTextStyleTokens = getIt<DesignTextStyleTokens>();
  final _loginUseCase = getIt<LoginUseCase>();
  final _connectivityRepository = getIt<ConnectivityRepository>();

  late TextEditingController _controllerEmail;
  late TextEditingController _controllerPassword;

  bool _loginAvailable = false;
  bool _loginInProgress = false;
  bool _connectionError = false;

  @override
  void initState() {
    super.initState();
    _connectionError = _connectivityRepository.getStatus() == ConnectivityResult.none;
    _connectivityRepository.addListener(onConnectionChanged);

    _controllerEmail = TextEditingController();
    _controllerPassword = TextEditingController();

    // fill email and password. ONLY for test!
    _controllerEmail.text = Constants.testEmail;
    _controllerPassword.text = Constants.testPassword;
    _updateLoginAvailable();
  }

  @override
  void dispose() {
    _controllerEmail.dispose();
    _controllerPassword.dispose();
    _connectivityRepository.removeListener(onConnectionChanged);
    super.dispose();
  }

  void onConnectionChanged(ConnectivityResult value) {
    setState(() {
      _connectionError = value == ConnectivityResult.none;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundMainColor,
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(child: Container()),
          Padding(
            padding: const EdgeInsets.all(16),
            child: TextField(
              controller: _controllerEmail,
              style: _designTextStyleTokens.textField(),
              enabled: !_loginInProgress,
              keyboardType: TextInputType.emailAddress,
              onChanged: (value) => _updateLoginAvailable(),
              cursorColor: AppColors.textMainColor,
              decoration: const InputDecoration(
                hintText: "Enter your email",
                labelText: "Email",
                labelStyle: TextStyle(
                  color: AppColors.textMainColor,
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16),
            child: TextField(
              controller: _controllerPassword,
              style: _designTextStyleTokens.textField(),
              enabled: !_loginInProgress,
              onChanged: (value) => _updateLoginAvailable(),
              cursorColor: AppColors.textMainColor,
              obscureText: true,
              obscuringCharacter: "*",
              enableSuggestions: false,
              autocorrect: false,
              decoration: const InputDecoration(
                hintText: "Enter your password",
                labelText: "Password",
                labelStyle: TextStyle(
                  color: AppColors.textMainColor,
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
              ),
            ),
          ),
          Expanded(child: Container()),
          Padding(
            padding: const EdgeInsets.all(16),
            child: MaterialButton(
              color: AppColors.buttonBackgroundColor,
              child: _loginInProgress
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Padding(
                          padding: EdgeInsets.all(4),
                          child: SizedBox(
                            width: 16,
                            height: 16,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 4),
                          child: Text(
                            'Login ... ',
                            style: _designTextStyleTokens.buttonActiveTitle(),
                          ),
                        )
                      ],
                    )
                  : Text(
                      'Login',
                      style: _loginAvailable ? _designTextStyleTokens.buttonActiveTitle() : _designTextStyleTokens.buttonInactiveTitle(),
                    ),
              onPressed: _loginAvailable
                  ? () {
                      if (!_loginInProgress && !_connectionError) {
                        final String email = _controllerEmail.text;
                        final String password = _controllerPassword.text;
                        _processLogin(
                          email: email,
                          password: password,
                        );
                      }
                    }
                  : null,
            ),
          ),
          if (_connectionError) ...[
            Container(
              padding: const EdgeInsets.all(16),
              decoration: const BoxDecoration(color: AppColors.textAccentColor),
              alignment: Alignment.center,
              child: Text(
                'No internet connection',
                style: _designTextStyleTokens.buttonInactiveTitle(),
              ),
            )
          ]
        ],
      ),
    );
  }

  void _processLogin({required String email, required String password}) {
    setState(() {
      _loginInProgress = true;
    });

    _loginUseCase.executeAsync(email: email, password: password).then((value) {
      if (value) {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const FeedScreen()));
      } else {
        Fluttertoast.showToast(
            msg: "Unable to login. Please check email and password!",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }).whenComplete(() {
      _resetState();
    });
  }

  void _resetState() {
    setState(() {
      _loginInProgress = false;
      _loginAvailable = _controllerEmail.text.isNotEmpty && _controllerPassword.text.isNotEmpty;
    });
  }

  void _updateLoginAvailable() {
    bool available = _controllerEmail.text.isNotEmpty && _controllerPassword.text.isNotEmpty;

    if (available != _loginAvailable) {
      setState(() {
        _loginAvailable = available;
      });
    }
  }
}
