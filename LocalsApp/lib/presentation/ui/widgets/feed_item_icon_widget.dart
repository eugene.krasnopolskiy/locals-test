import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:locals_app/design/app_colors.dart';

class FeedItemIconWidget extends StatelessWidget {
  final String? iconUrl;
  final double size;
  final bool isSvgIcon;

  const FeedItemIconWidget({
    Key? key,
    this.iconUrl,
    required this.size,
    this.isSvgIcon = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (iconUrl != null) {
      return Container(
        decoration: BoxDecoration(
          border: const Border(
            top: BorderSide(
              color: AppColors.textSecondaryColor,
              width: 0.5,
            ),
            right: BorderSide(
              color: AppColors.textSecondaryColor,
              width: 0.5,
            ),
            bottom: BorderSide(
              color: AppColors.textSecondaryColor,
              width: 0.5,
            ),
            left: BorderSide(
              color: AppColors.textSecondaryColor,
              width: 0.5,
            ),
          ),
          borderRadius: BorderRadius.circular(size),
        ),
        child: ClipOval(
          child: isSvgIcon
              ? SvgPicture.network(
                  iconUrl!,
                  width: size,
                  height: size,
                )
              : CachedNetworkImage(
                  width: size,
                  height: size,
                  fit: BoxFit.cover,
                  imageUrl: iconUrl!,
                  placeholder: (context, url) => _buildPlaceHolder(),
                  errorWidget: (context, url, error) => _buildPlaceHolder(),
                ),
        ),
      );
    }

    return _buildPlaceHolder();
  }

  Widget _buildPlaceHolder() {
    return Container(
      decoration: BoxDecoration(
        border: const Border(
          top: BorderSide(
            color: AppColors.textSecondaryColor,
            width: 0.5,
          ),
          right: BorderSide(
            color: AppColors.textSecondaryColor,
            width: 0.5,
          ),
          bottom: BorderSide(
            color: AppColors.textSecondaryColor,
            width: 0.5,
          ),
          left: BorderSide(
            color: AppColors.textSecondaryColor,
            width: 0.5,
          ),
        ),
        borderRadius: BorderRadius.circular(size),
        color: AppColors.textMainColor,
      ),
      width: size,
      height: size,
    );
  }
}
