import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:locals_app/design/app_colors.dart';
import 'package:locals_app/design/design_text_style_tokens.dart';
import 'package:locals_app/domain/entity/feed_item.dart';
import 'package:locals_app/presentation/ui/widgets/feed_item_icon_widget.dart';

class FeedItemWidget extends StatefulWidget {
  final FeedItem feedItem;
  final DesignTextStyleTokens designTextStyleTokens;
  final GestureTapCallback? onTap;

  const FeedItemWidget({
    required this.feedItem,
    required this.designTextStyleTokens,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  _FeedItemWidgetState createState() => _FeedItemWidgetState();
}

class _FeedItemWidgetState extends State<FeedItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppColors.backgroundMainColor),
      padding: const EdgeInsets.all(8),
      child: GestureDetector(
        onTap: widget.onTap,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                FeedItemIconWidget(
                  size: 48,
                  iconUrl: widget.feedItem.authorAvatarUrl,
                  isSvgIcon: widget.feedItem.authorAvatarExtension.toLowerCase().contains('svg'),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: Text(
                          widget.feedItem.title,
                          maxLines: 1,
                          textAlign: TextAlign.start,
                          style: widget.designTextStyleTokens.feedInfo(),
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Text(
                          _formatTimeLabel(widget.feedItem.timestamp),
                          maxLines: 1,
                          textAlign: TextAlign.start,
                          style: widget.designTextStyleTokens.feedTime(),
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            if (widget.feedItem.title.isNotEmpty) ...[
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Text(
                  _prepareText(widget.feedItem.title),
                  maxLines: 1,
                  textAlign: TextAlign.left,
                  style: widget.designTextStyleTokens.feedTitle(),
                ),
              ),
            ],
            if (widget.feedItem.text.isNotEmpty) ...[
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Text(
                  _prepareText(widget.feedItem.text),
                  maxLines: 1,
                  style: widget.designTextStyleTokens.feedText(),
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }

  String _formatTimeLabel(DateTime value) {
    return DateFormat.yMEd().add_jms().format(value);
  }

  String _prepareText(String text) {
    return text.replaceAll('\r\n', '');
  }
}
