import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:locals_app/domain/repository/connectivity/connectivity_repository.dart';
import 'package:locals_app/domain/repository/general/exception_handler.dart';
import 'package:locals_app/general/constants.dart';
import 'package:locals_app/presentation/injection/configure_dependencies.dart';
import 'package:locals_app/presentation/ui/locals_app.dart';

void main() {
  runZonedGuarded(() {
    WidgetsFlutterBinding.ensureInitialized();
    Future<void> configure;

    if (kReleaseMode) {
      configure = configureDependencies(Environment.prod);
    } else {
      configure = configureDependencies(Environment.dev);
    }
    configure.whenComplete(() {
      // sure that all dependencies injected
      GetIt.instance.allReady().whenComplete(() {
        SystemChrome.setPreferredOrientations(Constants.orientations).whenComplete(() {
          final connectivityRepository = getIt<ConnectivityRepository>();
          WidgetsBinding.instance?.addObserver(LifecycleEventHandler(connectivityRepository));
          connectivityRepository.init();
          runApp(const LocalsApp());
        });
      });
    });
  }, (Object error, StackTrace stack) {
    final ExceptionHandler handler = getIt<ExceptionHandler>();
    handler.processException(error, stack);
  });
}

class LifecycleEventHandler extends WidgetsBindingObserver {
  final ConnectivityRepository connectivityRepository;

  LifecycleEventHandler(this.connectivityRepository);

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
        await connectivityRepository.release();
        break;
      case AppLifecycleState.detached:
      case AppLifecycleState.resumed:
        await connectivityRepository.init();
        break;
    }
  }
}
